## To test the app, follow these steps:

1. Open Terminal.
2. Enter $ sudo gem install cocoapods command in terminal.
3. Navigate to directory containing Xcode project. Use cd “../directory-location/..” or cd [drag-and-drop project folder]
4. Run $ pod install
5. Open MYDOG.xcworkspace and build

