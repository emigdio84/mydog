//
//  ChooseFavoriteVC.m
//  MYDOG
//
//  Created by EMIGDIO CAMACHO CALDERON on 11/15/20.
//

#import "ChooseFavoriteVC.h"
#import "ServiceNetwork.h"
#import "SDWebImage.h"
#import "GlobalData.h"
#import "BreedCell.h"
#import "Utils.h"

@interface ChooseFavoriteVC() {
    
    NSMutableArray *breeds;
    
}
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *spinner;

@property (weak, nonatomic) IBOutlet UITableView *tableView;



@end

@implementation ChooseFavoriteVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    breeds = [[NSMutableArray alloc]init];
    
    [ServiceNetwork.sharedInstance getAllBreeds];

    [self.tableView setDelegate:self];
    [self.tableView setDataSource:self];
    [self.tableView registerNib:[UINib nibWithNibName:BREEDCELL
                                               bundle:nil] forCellReuseIdentifier:BREEDCELL];
    
    // Register this controller as an observer for the ALL_BREEDS_ARRIVED notification
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loadAllBreeds:) name:ALL_BREEDS_ARRIVED object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showError:) name:ERROR_ALL_BREEDS_NO_ARRIVED object:nil];
    
}

- (void) loadAllBreeds:(NSNotification*) notification{
    
    NSDictionary *breedsDict = notification.object;
    
    NSArray *nameBreeds = breedsDict.allKeys;
    
    for (int i = 0; i< nameBreeds.count; i++) {
        
        NSString *nameBreed = nameBreeds[i];
        
        [breeds addObject:nameBreed];
        
    }
    
    [self.spinner stopAnimating];
    
    [self.tableView reloadData];
    
}

-(void)showError:(NSNotification*) notification{
    
    [self.spinner stopAnimating];
    
    [Utils showConnectionProblems_AlertView:ERROR_MESSAGE];
    
}

#pragma mark - TableView Delegate

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 80;
}

#pragma mark - TableView Data Source

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return breeds.count;
    
}

- (UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
   
    BreedCell *cell = [tableView dequeueReusableCellWithIdentifier:BREEDCELL];
    
    NSString* breed = breeds[indexPath.row];
    
    cell.labelName.text = breed;
    
    return cell;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSString* breed = breeds[indexPath.row];
    
    [[NSUserDefaults standardUserDefaults] setObject:breed forKey:FAVORITE_BREED];
    
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [self.navigationController popViewControllerAnimated:YES];

}



@end
