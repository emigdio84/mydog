//
//  MyDogPhotoVC.m
//  MYDOG
//
//  Created by EMIGDIO CAMACHO CALDERON on 11/15/20.
//

#import "MyDogPhotoVC.h"

@interface MyDogPhotoVC ()
@property (weak, nonatomic) IBOutlet UIImageView *imagePhoto;
@property (weak, nonatomic) IBOutlet UIButton *buttonAddPhoto;

@end

@implementation MyDogPhotoVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.buttonAddPhoto.layer.cornerRadius = 10;
    self.buttonAddPhoto.layer.borderColor = [UIColor blackColor].CGColor;
    self.buttonAddPhoto.layer.borderWidth = 1;
    
}

- (IBAction)addPhotoMyDog:(UIButton *)sender {
    
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]){
        
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;

        [self presentViewController:picker animated:YES completion:nil];
        
    }
    
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {

    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    self.imagePhoto.image = chosenImage;
    
    UIImageWriteToSavedPhotosAlbum(chosenImage, nil, nil, nil);

    [picker dismissViewControllerAnimated:YES completion:nil];
    
}

@end
