//
//  BreedVC.m
//  MYDOG
//
//  Created by EMIGDIO CAMACHO CALDERON on 11/15/20.
//

#import "BreedVC.h"
#import "GlobalData.h"
#import "BreedCell.h"
#import "ImageBreedVC.h"

@interface BreedVC ()

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *labelNoSubBreeds;

@end

@implementation BreedVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.navigationItem setTitle:@"Sub Breeds list"];
    
    [self.tableView setDelegate:self];
    [self.tableView setDataSource:self];
    [self.tableView registerNib:[UINib nibWithNibName:BREEDCELL
                                               bundle:nil] forCellReuseIdentifier:BREEDCELL];
    
    if (self.subBreeds.count > 0){
        
        [self.labelNoSubBreeds setHidden:YES];
        
    }else{
        
        [self.labelNoSubBreeds setHidden:NO];
        
    }
    
    [self.tableView reloadData];
}

#pragma mark - TableView Delegate

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 80;
}

#pragma mark - TableView Data Source

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.subBreeds.count;
    
}

- (UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
   
    BreedCell *cell = [tableView dequeueReusableCellWithIdentifier:BREEDCELL];
    
    NSString* breed = self.subBreeds[indexPath.row];
    
    cell.labelName.text = breed;
    
    return cell;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSString* breed = self.subBreeds[indexPath.row];

    ImageBreedVC *imageBreedVC = [[ImageBreedVC alloc] initWithNibName:@"ImageBreedVC" bundle:nil];

    [imageBreedVC setBreed:breed];
    
    [self.navigationController pushViewController:imageBreedVC animated:YES];
    
}


@end
