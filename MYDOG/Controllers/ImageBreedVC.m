//
//  ImageBreedVC.m
//  MYDOG
//
//  Created by EMIGDIO CAMACHO CALDERON on 11/15/20.
//

#import "ImageBreedVC.h"
#import "ServiceNetwork.h"
#import "SDWebImage.h"
#import "GlobalData.h"
#import "Utils.h"

@interface ImageBreedVC ()

@property (weak, nonatomic) IBOutlet UIImageView *imageBreed;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *spinner;

@end

@implementation ImageBreedVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loadBreedImage:) name:IMAGE_RANDOM_BY_SUB_BREED_ARRIVED object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showError:) name:ERROR_IMAGE_RANDOM_BY_SUB_BREED_NO_ARRIVED object:nil];
    
    [ServiceNetwork.sharedInstance getGetRandomImageBySubBreed:self.breed];

}

- (void) loadBreedImage:(NSNotification*) notification{
    
    [self.spinner stopAnimating];
    
    NSString *strImageURL = notification.object;
    
    [self.imageBreed sd_setImageWithURL: [NSURL URLWithString: strImageURL]];
    
}

-(void)showError:(NSNotification*) notification{
    
    [self.spinner stopAnimating];
    
    [Utils showConnectionProblems_AlertView:ERROR_NO_IMAGE];
    
}

@end
