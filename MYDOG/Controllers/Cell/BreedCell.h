//
//  BreedCell.h
//  MYDOG
//
//  Created by EMIGDIO CAMACHO CALDERON on 11/15/20.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BreedCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *labelName;

@end

NS_ASSUME_NONNULL_END
