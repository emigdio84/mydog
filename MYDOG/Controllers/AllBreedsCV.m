//
//  AllBreedsCV.m
//  MYDOG
//
//  Created by EMIGDIO CAMACHO CALDERON on 11/15/20.
//

#import "AllBreedsCV.h"
#import "ServiceNetwork.h"
#import "GlobalData.h"
#import "BreedCell.h"
#import "BreedVC.h"
#import "Utils.h"

@interface AllBreedsCV (){
    
    NSMutableArray *breeds;
    
    NSDictionary *breedsDict;
}

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *spinner;

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation AllBreedsCV

- (void)viewDidLoad {
    [super viewDidLoad];
    
    breeds = [[NSMutableArray alloc]init];
    
    breedsDict = [[NSDictionary alloc]init];
    
    [ServiceNetwork.sharedInstance getAllBreeds];

    [self.tableView setDelegate:self];
    [self.tableView setDataSource:self];
    [self.tableView registerNib:[UINib nibWithNibName:BREEDCELL
                                               bundle:nil] forCellReuseIdentifier:BREEDCELL];
    
    // Register this controller as an observer for the ALL_BREEDS_ARRIVED notification
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loadAllBreeds:) name:ALL_BREEDS_ARRIVED object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showError:) name:ERROR_ALL_BREEDS_NO_ARRIVED object:nil];
    
    [self.navigationItem setTitle:@"Breeds list"];
    
}

- (void) loadAllBreeds:(NSNotification*) notification{
    
    breedsDict = notification.object;
    
    NSArray *nameBreeds = breedsDict.allKeys;
    
    for (int i = 0; i< nameBreeds.count; i++) {
        
        NSString *nameBreed = nameBreeds[i];
        
        [breeds addObject:nameBreed];
        
    }
    
    [self.spinner stopAnimating];
    
    [self.tableView reloadData];
    
}

-(void)showError:(NSNotification*) notification{
    
    [self.spinner stopAnimating];
    
    [Utils showConnectionProblems_AlertView:ERROR_MESSAGE];
    
}

#pragma mark - TableView Delegate

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 80;
}

#pragma mark - TableView Data Source

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return breeds.count;
    
}

- (UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
   
    BreedCell *cell = [tableView dequeueReusableCellWithIdentifier:BREEDCELL];
    
    NSString* breed = breeds[indexPath.row];
    
    cell.labelName.text = breed;
    
    return cell;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSString* breed = breeds[indexPath.row];
    
    NSArray* subBreeds = breedsDict[breed];
    
    BreedVC *breedVC = [[BreedVC alloc] initWithNibName:@"BreedVC" bundle:nil];

    [breedVC setSubBreeds:subBreeds];
    
    [self.navigationController pushViewController:breedVC animated:YES];
    
}

@end
