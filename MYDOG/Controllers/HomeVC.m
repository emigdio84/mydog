//
//  HomeVC.m
//  MYDOG
//
//  Created by EMIGDIO CAMACHO CALDERON on 11/14/20.
//

#import "HomeVC.h"
#import "ServiceNetwork.h"
#import "SDWebImage.h"
#import "GlobalData.h"
#import "Utils.h"

@interface HomeVC ()
@property (weak, nonatomic) IBOutlet UILabel *labelNameBreed;
@property (weak, nonatomic) IBOutlet UIImageView *imageFavorite;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *spinner;
@property (weak, nonatomic) IBOutlet UIButton *buttonChoose;

@end

@implementation HomeVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.buttonChoose.layer.cornerRadius = 10;
    self.buttonChoose.layer.borderColor = [UIColor blackColor].CGColor;
    self.buttonChoose.layer.borderWidth = 1;
    
}

-(void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
    
    if  ([[NSUserDefaults standardUserDefaults] valueForKey:FAVORITE_BREED] == nil){
        
        // Register this controller as an observer for the IMAGE_RANDOM_ARRIVED notification
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loadImageRandom:) name:IMAGE_RANDOM_ARRIVED object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showError:) name:ERROR_IMAGE_RANDOM_NO_ARRIVED object:nil];
        
        [self.spinner startAnimating];
        
        [ServiceNetwork.sharedInstance getGetRandomImage];

    }else{
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loadBreedFavorite:) name:IMAGE_RANDOM_BY_BREED_ARRIVED object:nil];
        
        NSString *breedFavorite = [[NSUserDefaults standardUserDefaults] valueForKey:FAVORITE_BREED];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showError:) name:ERROR_IMAGE_RANDOM_BY_BREED_NO_ARRIVED object:nil];
        
        self.labelNameBreed.text = breedFavorite;
        
        [self.spinner startAnimating];
        
        [ServiceNetwork.sharedInstance getGetRandomImageByBreed:breedFavorite];
        
    }
    
    
}

- (void) loadBreedFavorite:(NSNotification*) notification{
    
    NSString *strImageURL = notification.object;
    
    [self.imageFavorite sd_setImageWithURL:[NSURL URLWithString: strImageURL] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
        [self.spinner stopAnimating];
    }];
    
    [self.imageFavorite sd_setImageWithURL: [NSURL URLWithString: strImageURL]];
    
}

- (void) loadImageRandom:(NSNotification*) notification{
    
    NSString *strImageURL = notification.object;
    
    [self.imageFavorite sd_setImageWithURL:[NSURL URLWithString: strImageURL] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
        [self.spinner stopAnimating];
    }];
    
}

-(void)showError:(NSNotification*) notification{
    
    [self.spinner stopAnimating];
    
    [Utils showConnectionProblems_AlertView:ERROR_MESSAGE];
    
}

@end
