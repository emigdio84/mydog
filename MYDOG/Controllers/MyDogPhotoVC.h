//
//  MyDogPhotoVC.h
//  MYDOG
//
//  Created by EMIGDIO CAMACHO CALDERON on 11/15/20.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MyDogPhotoVC : UIViewController <UINavigationControllerDelegate, UIImagePickerControllerDelegate>

@end

NS_ASSUME_NONNULL_END
