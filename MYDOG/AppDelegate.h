//
//  AppDelegate.h
//  MYDOG
//
//  Created by EMIGDIO CAMACHO CALDERON on 11/13/20.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

