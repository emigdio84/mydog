//
//  ServiceNetwork.m
//  MYDOG
//
//  Created by EMIGDIO CAMACHO CALDERON on 11/14/20.
//

#import "ServiceNetwork.h"
#import "GlobalData.h"

@import AFNetworking;

@implementation ServiceNetwork

+ (ServiceNetwork *) sharedInstance
{
    static ServiceNetwork *inst = nil;
    @synchronized(self)
    {
        if (!inst)
        {
            inst = [[self alloc] init];
        }
    }
    return inst;
    
}

- (void) getAllBreeds
{
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    
    [manager GET:DOG_BREEDS_LIST_WS parameters:nil headers:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSDictionary* jsonDic;
        
        if ([responseObject isKindOfClass:[NSDictionary class]]){
            
            jsonDic = responseObject;
            NSLog(@"%@",jsonDic);
            
            // Starting parser
            NSDictionary* breedsDict= [jsonDic valueForKey:MESSAGE];
            
            if(breedsDict != nil)
            {
      
                //notification when already is downloaded all the breeds.
                [[NSNotificationCenter defaultCenter] postNotification:[NSNotification notificationWithName:ALL_BREEDS_ARRIVED object:breedsDict]];
        
            }
            
            
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        [[NSNotificationCenter defaultCenter] postNotification:[NSNotification notificationWithName:ERROR_ALL_BREEDS_NO_ARRIVED object:nil]];
        
    }];
 
}

- (void) getGetRandomImage
{
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    
    [manager GET:GET_RANDOM_IMAGE_BREED_WS parameters:nil headers:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSDictionary* jsonDic;
        
        if ([responseObject isKindOfClass:[NSDictionary class]]){
            
            jsonDic = responseObject;
            NSLog(@"%@",jsonDic);
            
            // Starting parser
            NSString* image = [jsonDic valueForKey:MESSAGE];
            
            if(image != nil)
            {
      
                    //notification for download of thophies of current user or friends.
                    [[NSNotificationCenter defaultCenter] postNotification:[NSNotification notificationWithName:IMAGE_RANDOM_ARRIVED object:image]];
        
            }
            
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        [[NSNotificationCenter defaultCenter] postNotification:[NSNotification notificationWithName:ERROR_IMAGE_RANDOM_NO_ARRIVED object:nil]];
        
    }];
 
}

- (void) getGetRandomImageByBreed:(NSString*) breed
{
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    
    NSString *path = [[NSString alloc] initWithFormat:@"%@%@/images/random", BASE_WS, breed];
    
    [manager GET:path parameters:nil headers:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSDictionary* jsonDic;
        
        if ([responseObject isKindOfClass:[NSDictionary class]]){
            
            jsonDic = responseObject;
            NSLog(@"%@",jsonDic);
            
            // Starting parser
            NSString* image = [jsonDic valueForKey:MESSAGE];
            
            if(image != nil)
            {
      
                    //notification for download of thophies of current user or friends.
                    [[NSNotificationCenter defaultCenter] postNotification:[NSNotification notificationWithName:IMAGE_RANDOM_BY_BREED_ARRIVED object:image]];
        
            }
            
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        [[NSNotificationCenter defaultCenter] postNotification:[NSNotification notificationWithName:ERROR_IMAGE_RANDOM_BY_BREED_NO_ARRIVED object:nil]];
    }];
 
}

- (void) getGetRandomImageBySubBreed:(NSString*) breed
{
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    
    NSString *path = [[NSString alloc] initWithFormat:@"%@%@/images/random", BASE_SUB_BREED_WS, breed];
    
    [manager GET:path parameters:nil headers:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSDictionary* jsonDic;
        
        if ([responseObject isKindOfClass:[NSDictionary class]]){
            
            jsonDic = responseObject;
            NSLog(@"%@",jsonDic);
            
            // Starting parser
            NSString* image = [jsonDic valueForKey:MESSAGE];
            
            if(image != nil)
            {
      
                    //notification for download of thophies of current user or friends.
                    [[NSNotificationCenter defaultCenter] postNotification:[NSNotification notificationWithName:IMAGE_RANDOM_BY_SUB_BREED_ARRIVED object:image]];
        
            }
            
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        [[NSNotificationCenter defaultCenter] postNotification:[NSNotification notificationWithName:ERROR_IMAGE_RANDOM_BY_SUB_BREED_NO_ARRIVED object:nil]];
        
    }];
 
}


@end
