//
//  ServiceNetwork.h
//  MYDOG
//
//  Created by EMIGDIO CAMACHO CALDERON on 11/14/20.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ServiceNetwork : NSObject

+ (ServiceNetwork *) sharedInstance;

- (void) getAllBreeds;

- (void) getGetRandomImage;

- (void) getGetRandomImageByBreed:(NSString*) breed;

- (void) getGetRandomImageBySubBreed:(NSString*) breed;

@end

NS_ASSUME_NONNULL_END
