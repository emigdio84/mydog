//
//  Utils.h
//  MYDOG
//
//  Created by EMIGDIO CAMACHO CALDERON on 11/16/20.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface Utils : NSObject

+ (void) showConnectionProblems_AlertView:(NSString*) message;

@end

NS_ASSUME_NONNULL_END
