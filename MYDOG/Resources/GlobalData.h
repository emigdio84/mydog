//
//  GlobalData.h
//  MYDOG
//
//  Created by EMIGDIO CAMACHO CALDERON on 11/14/20.
//

#ifndef GlobalData_h
#define GlobalData_h


#endif /* GlobalData_h */


//url
static NSString* const DOG_BREEDS_LIST_WS = @"https://dog.ceo/api/breeds/list/all";
static NSString* const GET_RANDOM_IMAGE_BREED_WS = @"https://dog.ceo/api/breeds/image/random";
static NSString* const BASE_WS = @"https://dog.ceo/api/breed/";
static NSString* const BASE_SUB_BREED_WS = @"https://dog.ceo/api/breed/hound/";

//keys
static NSString* const FAVORITE_BREED = @"FAVORITE_BREEDS";

static NSString* const IMAGE_RANDOM_ARRIVED = @"IMAGE_RANDOM_ARRIVED";

static NSString* const ERROR_IMAGE_RANDOM_NO_ARRIVED = @"ERROR_IMAGE_RANDOM_NO_ARRIVED";

static NSString* const IMAGE_RANDOM_BY_BREED_ARRIVED = @"IMAGE_RANDOM_BY_BREED_ARRIVED";

static NSString* const ERROR_IMAGE_RANDOM_BY_BREED_NO_ARRIVED = @"ERROR_IMAGE_RANDOM_BY_BREED_NO_ARRIVED";

static NSString* const IMAGE_RANDOM_BY_SUB_BREED_ARRIVED = @"IMAGE_RANDOM_BY_SUB_BREED_ARRIVED";

static NSString* const ERROR_IMAGE_RANDOM_BY_SUB_BREED_NO_ARRIVED = @"ERROR_IMAGE_RANDOM_BY_SUB_BREED_NO_ARRIVED";

static NSString* const ALL_BREEDS_ARRIVED = @"ALL_BREEDS_ARRIVED";

static NSString* const ERROR_ALL_BREEDS_NO_ARRIVED = @"ERROR_ALL_BREEDS_NO_ARRIVED";

static NSString* const MESSAGE = @"message";

static NSString* const ERROR_MESSAGE = @"An error has occurred";

static NSString* const ERROR_NO_IMAGE = @"No image available";

static NSString* const BREEDCELL = @"BreedCell";
