//
//  Utils.m
//  MYDOG
//
//  Created by EMIGDIO CAMACHO CALDERON on 11/16/20.
//

#import "Utils.h"
#import <UIKit/UIKit.h>
@implementation Utils

/**
 * It displays a message to the user saying that there's connection problems.
 *
 */
+ (void) showConnectionProblems_AlertView:(NSString*) message
{
    NSString* CONNECTION_PROBLEM_ALERTVIEW_TITLE = @"My Dog";

        
    UIAlertController * alert = [UIAlertController
                    alertControllerWithTitle:CONNECTION_PROBLEM_ALERTVIEW_TITLE
                                     message:message
                              preferredStyle:UIAlertControllerStyleAlert];



    UIAlertAction* okButton = [UIAlertAction
                        actionWithTitle:@"OK"
                                  style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action) {
                                    //Handle your yes please button action here
                                }];

    [alert addAction:okButton];

    [[[[UIApplication sharedApplication] keyWindow] rootViewController] presentViewController:alert animated:YES completion:^{
        }];
    
}

@end
